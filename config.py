import pygame           #importing all the libraries
import math             #

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
pygame.init()   


BROWN=[150,75,0]         #
BLUE=(135,206,235)       #`
WHITE=(255,255,255)      #giving respective colors their RGB value (to be used later)
BLACK=(0,0,0)            #
RED=(255,0,0)            #
screen=pygame.display.set_mode((1000,1000))         #width and height of screen
pygame.display.set_caption("Player_invasion")
sharko=[]               #declaring list for moving obstacles
fix_obs=[]          #
fix_obs2=[]         #above two are the lists for fixed obstacles
play1=[]
play2=[]
rnd1=1          #
rnd2=0          #variables to store the current round which is going for p1 and p2
check=1
fscor=[]        #list to store the final scores and time taken by each player in each round
w=[]
win1=0          #
win2=0          #variables to store number of wins of each player
ti=0
tot_scor1=0     #
tot_scor2=0     #variable to store the total score attained by each player over all the rounds
tot_time1=0     # 
tot_time2=0     #variable to store the total time taken by each player over all the rounds
pxchange=0          
pychange=0          
p2xchange=0
p2ychange=0
score1=0        #
score2=0        #variables to store the score of the current round
speed1=5            #
speed2=5            #speed of both players initialised to 5(initially)
p1=1            #denotes currently player 1 is playing
p2=0            #denotes currently player 2 is not playing
clock=pygame.time.Clock()
frame_count1=0  #
frame_count2=0  #used in calculating the time consumed by the player
sec1=0
sec2=0
frame_rate=60

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
play1.append(pygame.image.load('man.png'))   #adding image of player 1
play2.append(pygame.image.load('octopus.png'))
play1.extend((470,968))     #extending it to also store the initial x and y coordinates of player 1
play2.extend((600,5))
font=pygame.font.Font('freesansbold.ttf',16)
font1=pygame.font.Font('freesansbold.ttf',32)
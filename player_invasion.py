from config import *


for i in range(9):
    fix_obs.append([])
    fix_obs[i].append(pygame.image.load("ninja-shuriken.png"))
for i in range(6):
    fix_obs2.append([])
    fix_obs2[i].append(pygame.image.load("miscellaneous.png"))
for i in range(11):
    sharko.append([])
    sharko[i].append(pygame.image.load("shark.png"))


sharko[0].extend((150, 40, 0, 0))  #
sharko[1].extend((600, 80, 0, 0))  #
sharko[2].extend((50, 240, 0, 0))  #
sharko[3].extend((450, 310, 0, 0)) #
sharko[4].extend((700, 280, 0, 0)) # initializing all the moving obstacles to
sharko[5].extend((150, 490, 0, 0)) # a starting point
sharko[6].extend((850, 440, 0, 0)) #
sharko[7].extend((480, 665, 0, 0)) #
sharko[8].extend((75, 815, 0, 0))  #
sharko[9].extend((400, 865, 0, 0)) #
sharko[10].extend((800, 885, 0, 0))#

fix_obs[0].extend((430, -15, 0, 0))  #
fix_obs[1].extend((230, -15, 0, 0))  #
fix_obs[2].extend((150, 180, 0, 0))  #
fix_obs[3].extend((850, 180, 0, 0))  #
fix_obs[4].extend((375, 375, 0, 0))  #
fix_obs[5].extend((20, 570, 0, 0))   #
fix_obs[6].extend((750, 765, 0, 0))  #
fix_obs[7].extend((310, 955, 0, 0))  # initialising all fixed obstacles to
fix_obs[8].extend((550, 570, 0, 0))  # a starting point
fix_obs2[0].extend((430, 170, 0, 0)) #
fix_obs2[1].extend((750, 365, 0, 0)) #
fix_obs2[2].extend((310, 560, 0, 0)) #
fix_obs2[3].extend((910, 560, 0, 0)) #
fix_obs2[4].extend((170, 755, 0, 0)) #
fix_obs2[5].extend((660, 945, 0, 0)) #

# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
def show_score1(se):
    score = font.render("Player 1 (Score : " + str(score1), True, WHITE)  
    time = font.render(
        "Time : " + str(se) + ")", True, WHITE
    )                                                       # code snippet to display the score of p1
    screen.blit(score, (5, 977))                            # at the lower left corner
    screen.blit(time, (170, 977))                           #


def show_score2(s):
    score = font.render("Player 2 (Score : " + str(score2), True, (255, 255, 255))  
    time = font.render(
        "Time : " + str(s) + ")", True, (255, 255, 255)
    )                                                       # code snippet to display the score of p2
    screen.blit(score, (760, 5))                            # at the upper right corner
    screen.blit(time, (920, 5))                             #


# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
def f_obs_bri():
    for i in range(9):  
        screen.blit(
            fix_obs[i][0], (fix_obs[i][1], fix_obs[i][2])
        )                                                   # this part of code is responsible for displaying
    for i in range(6):                                      # the fixed obstacles that is the shuriken n the other 1
        screen.blit(fix_obs2[i][0], (fix_obs2[i][1], fix_obs2[i][2]))  


def moving_obstacle(speed):
    for i in range(11):                                          #
        screen.blit(sharko[i][0], (sharko[i][1], sharko[i][2]))  #
    for i in range(0, 11, 3):                                    # this part displays the moving obstacles and this
        sharko[i][1] = (
            sharko[i][1] + speed
        ) % 940                                                  # function takes a parameter to check the speed of
    for i in range(1, 11, 3):                                    # the moving obstacles
        sharko[i][1] = (sharko[i][1] + speed + 1) % 940          #
    for i in range(2, 11, 3):                                    #
        sharko[i][1] = (sharko[i][1] + speed - 1) % 940          #


# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
def play1img():  
    screen.blit(
        play1[0], (play1[1], play1[2])
    )                                                           # these functions are responsible for the


def play2img():                                                 # display of the player images stored in the list play(1,2)
    screen.blit(play2[0], (play2[1], play2[2]))                 #


# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
def scor1_incre():                                              #
    global score1, rnd1                                         #
    for i in range(9):                                          #       
        if fix_obs[i][3] < rnd1:                                #
            if play1[2] <= (
                fix_obs[i][2] - 32
            ):                                                  # this code snippet calculates the score of the players
                score1 += 5                                     # to calculate i am looping over all the obstacles both
                fix_obs[i][
                    3
                ] = rnd1                                        # fixed and moving and whenever my player crosses any
    for i in range(6):                                          # fixed obstacle the score(for that player) is incremented
        if (
            fix_obs2[i][3] < rnd1
        ):                                                      # by 5 and when the same happens for moving the score is
            if play1[2] <= (fix_obs2[i][2] - 32):               # incremented by 10
                score1 += 5                                     #
                fix_obs2[i][3] = rnd1                           #
    for i in range(11):                                         #
        if sharko[i][3] < rnd1:                                 #
            if play1[2] <= (sharko[i][2] - 32):                 #
                score1 += 10                                    #
                sharko[i][3] = rnd1                             #


def scor2_incre():                                              #
    global score2, rnd2                                         #
    for i in range(9):                                          #
        if fix_obs[i][4] < rnd2:                                #
            if play2[2] >= (fix_obs[i][2] + 32):                #
                score2 += 5                                     #
                fix_obs[i][4] = rnd2                            #
    for i in range(6):                                          # same as immiediate above snippet
        if fix_obs2[i][4] < rnd2:                               #
            if play2[2] >= (fix_obs2[i][2] + 32):               #
                score2 += 5                                     #
                fix_obs2[i][4] = rnd2                           #
    for i in range(11):                                         #
        if sharko[i][4] < rnd2:                                 #
            if play2[2] >= (sharko[i][2] + 32):                 #
                score2 += 10                                    #
                sharko[i][4] = rnd2                             #


# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
def display():
    gr = font.render("ROUND " + str(rnd1), True, (WHITE))       #
    if p1 == 1:                                                 #
        st = font.render("START", True, (BLACK))                #
        en = font.render(
            "END", True, (BLACK)
        )                                                       # this part is responsible for displaying
    else:                                                       # the current ongoing round on the top left
        st = font.render(
            "END", True, (BLACK)
        )                                                       # corner and the starting and the ending slab
        en = font.render(
            "START", True, (BLACK)
        )                                                       # for current player thta is depending on either
    screen.blit(st, (550, 980))                                 # it is 1 or 2
    screen.blit(en, (550, 5))                                   #
    screen.blit(gr, (5, 5))                                     #


# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
def detect_collision():
    global p1, p2, p2xchange, p2ychange, pxchange, pychange, rnd1, rnd2, score2, score1, sec1, sec2, frame_count2, frame_count1, check, speed1, speed2
    if p1 == 1:                                                 # does the calculation for player 1
        for i in range(11):                                     # THIS FUNCTION DETECTS COLLISION USING DISTANCE FORMULA
            x = math.sqrt(
                pow(sharko[i][1] - play1[1] + 16, 2)
                + pow(sharko[i][2] - play1[2] + 16, 2)
            )                     #
            if x < 48:            #
                p1 = 0            # checks distance between the moving obstacles and if it
                p2 = 1            # is less than 48 then my collision is detected and then
                play2[1] = 600    # i change my player reinitialise my new player to its
                play2[2] = 5      # original starting position
                p2xchange = 0     #
                p2ychange = 0     #
                rnd2 = rnd2 + 1   # also the round is incremented for that player
                score2 = 0        # the previous score value is resetted again to zero
                frame_count2 = 0  # to reinitialze the time for that player
        for i in range(9):
            x = math.sqrt(
                pow(fix_obs[i][1] - play1[1] + 16, 2)
                + pow(fix_obs[i][2] - play1[2] + 16, 2)
            )
            if x < 48:          #
                p1 = 0          #
                p2 = 1          #
                play2[1] = 600  #
                play2[2] = 5    # this part does the same thing as above part does
                p2xchange = 0   # but for fixed obstacle type 1
                p2ychange = 0   #
                rnd2 = rnd2 + 1 #
                score2 = 0      #
                frame_count2 = 0#
        for i in range(6):
            x = math.sqrt(
                pow(fix_obs2[i][1] - play1[1] + 16, 2)
                + pow(fix_obs2[i][2] - play1[2] + 16, 2)
            )
            if x < 48:          #
                p1 = 0          #
                p2 = 1          #
                play2[1] = 600  #
                play2[2] = 5    # this part does the above thing for fixed obstacle
                p2xchange = 0   # type 2
                p2ychange = 0   #
                rnd2 = rnd2 + 1 #
                score2 = 0      #
                frame_count2 = 0#
    else:                               # else condition does the calcuation if player 2 is playing
        for i in range(11):
            x = math.sqrt(
                pow(sharko[i][1] - play2[1] + 16, 2)
                + pow(sharko[i][2] - play2[2] + 16, 2)
            )
            if x < 48:
                p1 = 1
                p2 = 0
                play1[1] = 470
                play1[2] = 968
                pxchange = 0
                pychange = 0
                rnd1 = rnd1 + 1
                fscor.append([])
                fscor[rnd2 - 1].extend((score1, score2, sec1, sec2))
                if score1 > score2:
                    speed1 = speed1 + 1
                elif score2 > score1:
                    speed2 = speed2 + 1
                else:
                    if sec1 < sec2:
                        speed1 = speed1 + 1
                    elif sec2 < sec1:
                        speed2 = speed2 + 1
                score1 = 0
                frame_count1 = 0
                if rnd1 > 11:
                    check = 0
        for i in range(9):
            x = math.sqrt(
                pow(fix_obs[i][1] - play2[1] + 16, 2)
                + pow(fix_obs[i][2] - play2[2] + 16, 2)
            )
            if x < 48:
                p1 = 1
                p2 = 0
                play1[1] = 470
                play1[2] = 968
                pxchange = 0
                pychange = 0
                rnd1 = rnd1 + 1
                fscor.append([])
                fscor[rnd2 - 1].extend((score1, score2, sec1, sec2))
                if score1 > score2:
                    speed1 = speed1 + 1
                elif score2 > score1:
                    speed2 = speed2 + 1
                else:
                    if sec1 < sec2:
                        speed1 = speed1 + 1
                    elif sec2 < sec1:
                        speed2 = speed2 + 1
                score1 = 0
                frame_count1 = 0
                if rnd1 > 11:
                    check = 0
        for i in range(6):
            x = math.sqrt(
                pow(fix_obs2[i][1] - play2[1] + 16, 2)
                + pow(fix_obs2[i][2] - play2[2] + 16, 2)
            )
            if x < 48:
                p1 = 1
                p2 = 0
                play1[1] = 470
                play1[2] = 968
                pxchange = 0
                pychange = 0
                rnd1 = rnd1 + 1
                fscor.append([])
                fscor[rnd2 - 1].extend((score1, score2, sec1, sec2))
                if score1 > score2:
                    speed1 = speed1 + 1
                elif score2 > score1:
                    speed2 = speed2 + 1
                else:
                    if sec1 < sec2:
                        speed1 = speed1 + 1
                    elif sec2 < sec1:
                        speed2 = speed2 + 1
                score1 = 0
                frame_count1 = 0
                if rnd1 > 11:
                    check = 0

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
def calculate_results():
    global tot_scor1, tot_scor2, tot_time1, tot_time2, win1, win2, rnd2
    if p2 == 1:
        rnd2 = rnd2 - 1                                         #this checks if esc key pressed in between a round that is p2 is playing
    for i in range(rnd2):                                       #
        tot_scor1 = tot_scor1 + fscor[i][0]                     #this part calculated total score and time consumned by both the players
        tot_scor2 = tot_scor2 + fscor[i][1]                     #all the rounds played by them
        tot_time1 = tot_time1 + fscor[i][2]                     #
        tot_time2 = tot_time2 + fscor[i][3]                     #the below code snippet is responsible for finding invidual round winners
        if fscor[i][0] > fscor[i][1]:                           #
            w.append(
                "Player 1 wins on basis of score in round "
                + str(i + 1)
                + " ( p1_score:"
                + str(fscor[i][0])
                + "    p2_score:"
                + str(fscor[i][1])
                + ")"
            )
            win1 = win1 + 1
        elif fscor[i][0] < fscor[i][1]:
            w.append(
                "Player 2 wins on basis of score in round "
                + str(i + 1)
                + " ( p1_score:"
                + str(fscor[i][0])
                + "    p2_score:"
                + str(fscor[i][1])
                + ")"
            )
            win2 = win2 + 1
        else:
            if fscor[i][2] < fscor[i][3]:
                w.append(
                    "Player 1 wins on basis of time(because of same score) in round "
                    + str(i + 1)
                    + " ( p1_time:"
                    + str(fscor[i][2])
                    + "    p2_time:"
                    + str(fscor[i][3])
                    + "   score:"
                    + str(fscor[i][0])
                    + ")"
                )
                win1 = win1 + 1
            elif fscor[i][2] > fscor[i][3]:
                w.append(
                    "Player 2 wins on basis of time(because of same score) in round "
                    + str(i + 1)
                    + " ( p1_time:"
                    + str(fscor[i][2])
                    + "    p2_time:"
                    + str(fscor[i][3])
                    + "   score:"
                    + str(fscor[i][0])
                    + ")"
                )
                win2 = win2 + 1
            else:
                w.append(
                    "There is a tie because of same score and time in round "
                    + str(i + 1)
                    + " ( score:"
                    + str(fscor[i][0])
                    + "   time:"
                    + str(fscor[i][2])
                    + ")"
                )

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#
def display_results():                                                                  #
    global rnd2, win1, win2, tot_time2, tot_time1, tot_scor2, tot_scor1                 #this part displays the results calculated in the above function
    screen.blit(font1.render("RESULTS!!!", True, WHITE), (430, 20))                     #by accessing the w list created in the above part
    for i in range(rnd2):                                                               #
        screen.blit(font.render(w[i], True, BLACK), (60, 60 + (20 * i)))                #
    if win1 > win2:                                     
        screen.blit(
            font.render(
                "Overall Player 1 wins because of total number of more wins "
                + "(p1_wins:"
                + str(win1)
                + "   p2_wins:"
                + str(win2)
                + ")",
                True,
                RED,
            ),
            (60, 120 + (20 * rnd2)),
        )
    elif win2 > win1:
        screen.blit(
            font.render(
                "Overall Player 2 wins because of total number of more wins "
                + "(p1_wins:"
                + str(win1)
                + "   p2_wins:"
                + str(win2)
                + ")",
                True,
                RED,
            ),
            (60, 120 + (20 * rnd2)),
        )
    else:
        if tot_scor1 > tot_scor2:
            screen.blit(
                font.render(
                    "Overall Player 1 wins because of more total score "
                    + "(p1_tot_scor:"
                    + str(tot_scor1)
                    + "  p2_tot_scor:"
                    + str(tot_scor2)
                    + ")",
                    True,
                    RED,
                ),
                (60, 120 + (20 * rnd2)),
            )
        elif tot_scor1 < tot_scor2:
            screen.blit(
                font.render(
                    "Overall Player 2 wins because of more total score "
                    + "(p1_tot_scor:"
                    + str(tot_scor1)
                    + "  p2_tot_scor:"
                    + str(tot_scor2)
                    + ")",
                    True,
                    RED,
                ),
                (60, 120 + (20 * rnd2)),
            )
        else:
            if tot_time1 < tot_time2:
                screen.blit(
                    font.render(
                        "Overall Player 1 wins because of less total time(total score of both players is same) "
                        + "{tot_scor:"
                        + str(tot_scor1)
                        + "  p1_tot_time:"
                        + str(tot_time1)
                        + "   p2_tot_time"
                        + str(tot_time2)
                        + "}",
                        True,
                        RED,
                    ),
                    (60, 120 + (20 * rnd2)),
                )
            elif tot_time1 > tot_time2:
                screen.blit(
                    font.render(
                        "Overall Player 2 wins because of less total time(total score of both players is same) "
                        + "{tot_scor:"
                        + str(tot_scor1)
                        + "  p1_tot_time:"
                        + str(tot_time1)
                        + "   p2_tot_time"
                        + str(tot_time2)
                        + "}",
                        True,
                        RED,
                    ),
                    (60, 120 + (20 * rnd2)),
                )
            else:
                screen.blit(
                    font.render(
                        "Overall there is a tie due to same total score and total time"
                        + "(tot_scor:"
                        + str(tot_scor1)
                        + "  tot_time:"
                        + str(tot_time1)
                        + ")",
                        True,
                        RED,
                    ),
                    (60, 120 + (20 * rnd2)),
                )

#++++++++++++++++++++++++++++++++++++++MAIN_WHILE_LOOP++++++++++++++++++++++++++++++++#
run = True
while run:
    for event in pygame.event.get():                                                #
        if event.type == pygame.QUIT:                                               #
            run = False                                                             #
        if p1 == 1:                                                                 #
            if event.type == pygame.KEYDOWN:                                        #
                if event.key == pygame.K_LEFT:                                      #
                    pxchange = -3                                                   #
                if event.key == pygame.K_RIGHT:                                     #
                    pxchange = +3                                                   #
                if event.key == pygame.K_UP:                                        #
                    pychange = -3                                                   #
                if event.key == pygame.K_DOWN:                                      #
                    pychange = +3                                                   #
            if event.type == pygame.KEYUP:                                          #
                if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:       #this part is responsible for detecting the keys pressed by the palyer
                    pxchange = 0                                                    #and based on that corresponding conditions for movement is set
                if event.key == pygame.K_UP or event.key == pygame.K_DOWN:          #
                    pychange = 0                                                    #
        if p2 == 1:                                                                 #
            if event.type == pygame.KEYDOWN:                                        #
                if event.key == pygame.K_a:                                         #
                    p2xchange = -3                                                  #
                if event.key == pygame.K_d:                                         #
                    p2xchange = +3                                                  #
                if event.key == pygame.K_w:                                         #
                    p2ychange = -3                                                  #
                if event.key == pygame.K_s:                                         #
                    p2ychange = +3                                                  #
            if event.type == pygame.KEYUP:                                          #
                if event.key == pygame.K_a or event.key == pygame.K_d:              #
                    p2xchange = 0                                                   #
                if event.key == pygame.K_w or event.key == pygame.K_s:              #
                    p2ychange = 0                                                   #
        if event.type == pygame.KEYDOWN:                                            #
            if event.key == pygame.K_ESCAPE:                                        #
                check = 0                                                           #

    if check == 1:
        if p1 == 1:
            play1[1] += pxchange                            #
            if play1[1] >= 968:                             #
                play1[1] = 968                              #
            if play1[1] <= 0:                               #
                play1[1] = 0                                #manages the part if my player has hit the side boundary of the game
            play1[2] += pychange                            #
            if play1[2] >= 968:                             #
                play1[2] = 968                              #
            if play1[2] <= 0:                               #
                play1[2] = 0                                #
        else:                                                   #manages the same for player 2
            play2[1] += p2xchange
            if play2[1] >= 968:
                play2[1] = 968
            if play2[1] <= 0:
                play2[1] = 0
            play2[2] += p2ychange
            if play2[2] >= 968:
                play2[2] = 968
            if play2[2] <= 0:
                play2[2] = 0

        screen.fill(BLUE)
        pygame.draw.rect(screen, BROWN, [0, 0, 1000, 25])                   #
        pygame.draw.rect(screen, BROWN, [0, 195, 1000, 25])                 #
        pygame.draw.rect(screen, BROWN, [0, 390, 1000, 25])                 #part to create partitiions on the screen
        pygame.draw.rect(screen, BROWN, [0, 585, 1000, 25])                 #
        pygame.draw.rect(screen, BROWN, [0, 780, 1000, 25])                 #
        pygame.draw.rect(screen, BROWN, [0, 975, 1000, 25])                 #
        display()

        if p1 == 1:
            sec1 = frame_count1 // frame_rate               #calculating time consumed in current round for player 1
            frame_count1 += 1
        else:
            sec2 = frame_count2 // frame_rate               #doing the above thing for player 2
            if play2[2] < 968:
                frame_count2 += 1
        clock.tick(frame_rate)

        f_obs_bri()

        if p1 == 1:
            moving_obstacle(speed1)
            play1img()
            scor1_incre()
        else:
            moving_obstacle(speed2)
            play2img()
            scor2_incre()

        detect_collision()
        show_score1(sec1)
        show_score2(sec2)

        if play1[2] <= 5:                           #
            p1 = 0                                  #
            p2 = 1                                  #
            play2[1] = 600                          #
            play2[2] = 5                            #
            play1[1] = 470                          #detecting if my player has crossed current round
            play1[2] = 968                          #
            p2xchange = 0                           #
            p2ychange = 0                           #
            rnd2 = rnd2 + 1                         #
            score2 = 0                              #
            frame_count2 = 0                        #
        if play2[2] >= 963:
            p1 = 1
            p2 = 0
            play2[1] = 600
            play2[2] = 5
            play1[1] = 470
            play1[2] = 968
            pxchange = 0
            pychange = 0
            rnd1 = rnd1 + 1
            fscor.append([])
            fscor[rnd2 - 1].extend((score1, score2, sec1, sec2))
            if score1 > score2:
                speed1 = speed1 + 1
            elif score2 > score1:
                speed2 = speed2 + 1
            else:
                if sec1 < sec2:
                    speed1 = speed1 + 1
                elif sec2 < sec1:
                    speed2 = speed2 + 1
            score1 = 0
            frame_count1 = 0
            if rnd1 > 11:
                check = 0
    else:
        screen.fill(BLUE)                       #
        if ti == 0:                             #
            calculate_results()                 #part which executes when escape key is pressed
            ti = 1                              #
        else:                                   #
            display_results()                   #
    pygame.display.update()
